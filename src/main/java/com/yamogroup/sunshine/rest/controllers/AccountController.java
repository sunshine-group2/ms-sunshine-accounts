package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.request.AccountDtoRequest;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.enums.TypeAccount;
import com.yamogroup.sunshine.exceptions.AccountException;
import com.yamogroup.sunshine.rest.api.AccountApi;
import com.yamogroup.sunshine.services.AccountService;
import com.yamogroup.sunshine.services.mappers.AccountConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class AccountController implements AccountApi {

  private final AccountService accountService;

  @Override
  public ResponseEntity<AccountDtoResponse> saveAccount(AccountDtoRequest accountDtoRequest) {
    return ResponseEntity.ok(
        accountService.saveAccount(AccountConverter.toEntity(accountDtoRequest)));
  }

  @Override
  public ResponseEntity<List<AccountDtoResponse>> listAccountByType(TypeAccount accountType) {
    return ResponseEntity.ok(accountService.listAccountByType(accountType));
  }

  @Override
  public ResponseEntity<AccountDtoResponse> findAccountByIdAndType(
      Long accountId, TypeAccount accountType) throws AccountException {
    return ResponseEntity.ok(accountService.findAccountByIdAndType(accountId, accountType));
  }

  @Override
  public ResponseEntity<AccountDtoResponse> updateAccount(AccountDtoRequest accountDtoRequest)
      throws AccountException {
    return ResponseEntity.ok(
        accountService.updateAccount(AccountConverter.toEntity(accountDtoRequest)));
  }

  @Override
  public ResponseEntity<AccountDtoResponse> getAccountById(Long accountId) throws AccountException {
    return ResponseEntity.ok(accountService.getAccountById(accountId));
  }

  @Override
  public ResponseEntity<AccountDtoResponse> findAccountByEmail(String email)
      throws AccountException {
    return ResponseEntity.ok(accountService.findAccountByEmail(email));
  }

  @Override
  public ResponseEntity<AccountDtoResponse> findAccountByPseudo(String pseudo)
      throws AccountException {
    return ResponseEntity.ok(accountService.findAccountByPseudo(pseudo));
  }

  @Override
  public ResponseEntity<List<AccountDtoResponse>> findAllAccount() throws AccountException {
    return ResponseEntity.ok(accountService.findAllAccount());
  }
}
