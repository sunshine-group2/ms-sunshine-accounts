package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.exceptions.ProductException;
import com.yamogroup.sunshine.rest.api.ProductApi;
import com.yamogroup.sunshine.services.ProductService;
import com.yamogroup.sunshine.services.mappers.ProductConverter;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class ProductController implements ProductApi {
    private final ProductService productService;
    @Override
    public ResponseEntity<ProductDto> updateProduct(ProductDto productDto) throws ProductException {
        return ResponseEntity.ok(productService.updateProduct(ProductConverter.toEntity(productDto)));
    }

    @Override
    public ResponseEntity<List<ProductDto>> findAllProducts() {
        return ResponseEntity.ok(productService.findAllProducts());
    }

    @Override
    public ResponseEntity<ProductDto> saveProduct(ProductDto productDto) throws ProductException {
        return ResponseEntity.ok(productService.saveProduct(ProductConverter.toEntity(productDto)));
    }
}
