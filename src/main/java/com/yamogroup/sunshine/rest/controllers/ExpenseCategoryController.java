package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.rest.api.ExpenseCategoryApi;
import com.yamogroup.sunshine.services.ExpenseCategoryService;
import com.yamogroup.sunshine.services.mappers.ExpenseCategoryConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class ExpenseCategoryController implements ExpenseCategoryApi {

  private final ExpenseCategoryService expenseCategoryService;

  @Override
  public ResponseEntity<ExpenseCategoryDto> saveExpenseCategory(
      ExpenseCategoryDto expenseCategoryDto) {
    return new ResponseEntity<>(
        expenseCategoryService.saveExpenseCategory(
            ExpenseCategoryConverter.toEntity(expenseCategoryDto)),
        HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<ExpenseCategoryDto> findExpenseCategoryById(Long accountId)
      throws ExpenseException {
    return ResponseEntity.ok(expenseCategoryService.findExpenseCategoryById(accountId));
  }

  @Override
  public ResponseEntity<ExpenseCategoryDto> updateExpenseCategory(
      @Valid ExpenseCategoryDto expenseCategoryDto) throws ExpenseException {
    return ResponseEntity.ok(expenseCategoryService.updateExpenseCategory(expenseCategoryDto));
  }

  @Override
  public ResponseEntity<List<ExpenseCategoryDto>> findAllExpenseCategory() {
    return ResponseEntity.ok(expenseCategoryService.findAllExpenseCategory());
  }
}
