package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.rest.api.ExpenseApi;
import com.yamogroup.sunshine.services.ExpenseService;
import com.yamogroup.sunshine.services.mappers.ExpenseConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class ExpenseController implements ExpenseApi {
  private final ExpenseService expenseService;

  @Override
  public ResponseEntity<ExpenseDto> saveExpense(ExpenseDto expenseDto) throws ExpenseException {
    return new ResponseEntity<>(
        expenseService.saveExpense(ExpenseConverter.toEntity(expenseDto)), HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<ExpenseDto> updateExpense(ExpenseDto expenseDto) throws ExpenseException {
    return ResponseEntity.ok(expenseService.updateExpense(ExpenseConverter.toEntity(expenseDto)));
  }
}
