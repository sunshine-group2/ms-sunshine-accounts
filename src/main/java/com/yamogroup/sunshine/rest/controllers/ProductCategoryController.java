package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.exceptions.ProductCategoryException;
import com.yamogroup.sunshine.rest.api.ProductCategoryApi;
import com.yamogroup.sunshine.services.ProductCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class ProductCategoryController implements ProductCategoryApi {

    private final ProductCategoryService productCategoryService;

    @Override
    public ResponseEntity<ProductCategoryDto> findProductCategoryById(Long categoryId)
            throws ProductCategoryException {
        return ResponseEntity.ok(productCategoryService.findProductCategoryById(categoryId));
    }

    @Override
    public ResponseEntity<List<ProductCategoryDto>> findAllCategories() {
        return ResponseEntity.ok(productCategoryService.findAllCategories());
    }

    @Override
    public ResponseEntity<ProductCategoryDto> updateProductCategory(ProductCategoryDto productCategoryDto) throws ProductCategoryException {
        return ResponseEntity.ok(productCategoryService.updateProductCategory(productCategoryDto));
    }
}
