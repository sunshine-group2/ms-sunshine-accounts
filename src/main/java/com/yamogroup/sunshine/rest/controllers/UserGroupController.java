package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.exceptions.UserGroupException;
import com.yamogroup.sunshine.rest.api.UserGroupApi;
import com.yamogroup.sunshine.services.UserGroupService;
import com.yamogroup.sunshine.services.mappers.UserGroupConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class UserGroupController implements UserGroupApi {

    private final UserGroupService userGroupService;

    @Override
    public ResponseEntity<UserGroupDto> saveUserGroup(UserGroupDto userGroupDto) throws UserGroupException {
        return new ResponseEntity<>(userGroupService.saveUserGroup(UserGroupConverter.toEntity(userGroupDto)), HttpStatus.CREATED);
    }
}
