package com.yamogroup.sunshine.rest.controllers;

import com.yamogroup.sunshine.dto.request.UserAddRequest;
import com.yamogroup.sunshine.dto.response.UserResponse;
import com.yamogroup.sunshine.rest.api.UserApi;
import com.yamogroup.sunshine.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

  private final AccountService accountService;

  @Override
  public ResponseEntity<UserResponse> addUser(UserAddRequest userAddRequest) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(accountService.addUser(userAddRequest.toEntity()));
  }
}
