package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.exceptions.UserGroupException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path = "/userGroup", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserGroupApi {
    @PostMapping
    ResponseEntity<UserGroupDto> saveUserGroup(@RequestBody UserGroupDto userGroupDto) throws UserGroupException;
}
