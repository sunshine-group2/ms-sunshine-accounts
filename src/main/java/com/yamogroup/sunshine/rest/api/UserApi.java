package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.request.UserAddRequest;
import com.yamogroup.sunshine.dto.response.UserResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserApi {

  @PostMapping
  ResponseEntity<UserResponse> addUser(@Valid @RequestBody UserAddRequest userAddRequest);
}
