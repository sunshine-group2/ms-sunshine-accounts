package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.exceptions.ProductException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProductApi {
  @PutMapping
  ResponseEntity<ProductDto> updateProduct(@Valid @RequestBody ProductDto productDto)
      throws ProductException;

  @GetMapping
  ResponseEntity<List<ProductDto>> findAllProducts();

  @PostMapping
  ResponseEntity<ProductDto> saveProduct(@Valid @RequestBody ProductDto productDto)
      throws ProductException;
}
