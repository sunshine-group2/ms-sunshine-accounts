package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(path = "/categoryExpenses", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ExpenseCategoryApi {

  @PostMapping
  ResponseEntity<ExpenseCategoryDto> saveExpenseCategory(
      @Valid @RequestBody ExpenseCategoryDto expenseCategoryDto);

  @GetMapping("{id}")
  ResponseEntity<ExpenseCategoryDto> findExpenseCategoryById(@PathVariable("id") Long accountId)
      throws ExpenseException;

  @PutMapping
  ResponseEntity<ExpenseCategoryDto> updateExpenseCategory(
      @Valid @RequestBody ExpenseCategoryDto expenseCategoryDto) throws ExpenseException;

  @GetMapping
  ResponseEntity<List<ExpenseCategoryDto>> findAllExpenseCategory();
}
