package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.exceptions.ProductCategoryException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "/productCategories", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProductCategoryApi {
  @GetMapping("{id}")
  ResponseEntity<ProductCategoryDto> findProductCategoryById(@PathVariable("id") Long categoryId)
      throws ProductCategoryException;

  @GetMapping
  ResponseEntity<List<ProductCategoryDto>> findAllCategories();

  @PutMapping
  ResponseEntity<ProductCategoryDto> updateProductCategory(@RequestBody ProductCategoryDto productCategoryDto) throws ProductCategoryException;

}
