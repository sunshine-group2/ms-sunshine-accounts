package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.AccountUpdate;
import com.yamogroup.sunshine.dto.request.AccountDtoRequest;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.enums.TypeAccount;
import com.yamogroup.sunshine.exceptions.AccountException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(path = "/account", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AccountApi {

  @PostMapping
  ResponseEntity<AccountDtoResponse> saveAccount(
      @Valid @RequestBody AccountDtoRequest accountDtoRequest);

  @GetMapping("/type/{type}")
  ResponseEntity<List<AccountDtoResponse>> listAccountByType(
      @PathVariable("type") TypeAccount accountType);

  @GetMapping("{id}/type/{type}")
  ResponseEntity<AccountDtoResponse> findAccountByIdAndType(
      @PathVariable("id") Long accountId, @PathVariable("type") TypeAccount accountType)
      throws AccountException;

  @PutMapping
  ResponseEntity<AccountDtoResponse> updateAccount(
      @Validated(AccountUpdate.class) @RequestBody AccountDtoRequest accountDtoRequest)
      throws AccountException;

  @GetMapping("{id}")
  ResponseEntity<AccountDtoResponse> getAccountById(@PathVariable("id") Long accountId)
      throws AccountException;

  @GetMapping("/email/{email}")
  ResponseEntity<AccountDtoResponse> findAccountByEmail(@PathVariable("email") String email)
      throws AccountException;

  @GetMapping("/pseudo/{pseudo}")
  ResponseEntity<AccountDtoResponse> findAccountByPseudo(@PathVariable("pseudo") String pseudo)
      throws AccountException;

  @GetMapping
  ResponseEntity<List<AccountDtoResponse>> findAllAccount() throws AccountException;
}
