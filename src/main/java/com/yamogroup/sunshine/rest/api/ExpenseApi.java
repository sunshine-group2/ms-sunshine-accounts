package com.yamogroup.sunshine.rest.api;

import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping(path = "/expenses", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ExpenseApi {
  @PostMapping
  ResponseEntity<ExpenseDto> saveExpense(@Valid @RequestBody ExpenseDto expenseDto)
      throws ExpenseException;

  @PutMapping
  ResponseEntity<ExpenseDto> updateExpense(@Valid @RequestBody ExpenseDto expenseDto)
      throws ExpenseException;
}
