package com.yamogroup.sunshine.services;

import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;

import java.util.List;

public interface ExpenseCategoryService {
  ExpenseCategoryDto saveExpenseCategory(ExpenseCategoryEntity expenseCategoryEntity);

  ExpenseCategoryDto findExpenseCategoryById(Long accountId) throws ExpenseException;

  List<ExpenseCategoryDto> findAllExpenseCategory();

  ExpenseCategoryDto updateExpenseCategory(ExpenseCategoryDto expenseCategoryDto)
      throws ExpenseException;
}
