package com.yamogroup.sunshine.services;

import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;

public interface ExpenseService {
  ExpenseDto saveExpense(ExpenseEntity entity) throws ExpenseException;

  ExpenseDto updateExpense(ExpenseEntity expenseEntity) throws ExpenseException;
}
