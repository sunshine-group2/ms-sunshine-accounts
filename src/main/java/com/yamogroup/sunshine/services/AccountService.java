package com.yamogroup.sunshine.services;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.dto.response.UserResponse;
import com.yamogroup.sunshine.enums.TypeAccount;
import com.yamogroup.sunshine.exceptions.AccountException;

import java.util.List;

public interface AccountService {

  AccountDtoResponse saveAccount(AccountEntity entity);

  List<AccountDtoResponse> listAccountByType(TypeAccount accountType);

  AccountDtoResponse findAccountByIdAndType(Long accountId, TypeAccount accountType)
      throws AccountException;

  AccountDtoResponse updateAccount(AccountEntity entity) throws AccountException;

  AccountDtoResponse getAccountById(Long accountId) throws AccountException;

  AccountDtoResponse findAccountByEmail(String email);

  AccountDtoResponse findAccountByPseudo(String pseudo);

  List<AccountDtoResponse> findAllAccount();

  UserResponse addUser(AccountEntity accountEntity);
}
