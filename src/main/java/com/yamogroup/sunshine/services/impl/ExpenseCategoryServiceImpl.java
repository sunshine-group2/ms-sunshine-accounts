package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dao.repositories.ExpenseCategoryRepository;
import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.services.ExpenseCategoryService;
import com.yamogroup.sunshine.services.mappers.ExpenseCategoryConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ExpenseCategoryServiceImpl implements ExpenseCategoryService {
  private final ExpenseCategoryRepository repository;

  @Override
  public ExpenseCategoryDto saveExpenseCategory(ExpenseCategoryEntity entity) {
    entity = repository.save(entity);
    return ExpenseCategoryConverter.toDto(entity);
  }

  @Override
  public ExpenseCategoryDto findExpenseCategoryById(Long accountId) throws ExpenseException {
    Optional<ExpenseCategoryEntity> optional = repository.findById(accountId);
    if (optional.isEmpty()) {
      throw new ExpenseException("La catégorie de dépense spécifiée n'existe pas");
    }
    return ExpenseCategoryConverter.toDto(optional.get());
  }

  @Override
  public List<ExpenseCategoryDto> findAllExpenseCategory() {
    return repository.findAll().parallelStream()
        .map(ExpenseCategoryConverter::toDto)
        .collect(Collectors.toList());
  }

  @Override
  public ExpenseCategoryDto updateExpenseCategory(ExpenseCategoryDto expenseCategoryDto)
      throws ExpenseException {
    Optional<ExpenseCategoryEntity> optional = repository.findById(expenseCategoryDto.getId());
    if (optional.isEmpty()) {
      throw new ExpenseException("Id manquant ou enregistrement manquant pour cette catégorie");
    }
    ExpenseCategoryEntity oldversion = optional.get();
    oldversion.update(ExpenseCategoryConverter.toEntity(expenseCategoryDto));
    oldversion = repository.save(oldversion);
    return ExpenseCategoryConverter.toDto(oldversion);
  }
}
