package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ProductEntity;
import com.yamogroup.sunshine.dao.repositories.ProductCategoryRepository;
import com.yamogroup.sunshine.dao.repositories.ProductRepository;
import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.exceptions.ProductException;
import com.yamogroup.sunshine.services.ProductService;
import com.yamogroup.sunshine.services.mappers.ProductConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final ProductCategoryRepository categoryRepository;
    @Override
    public ProductDto updateProduct(ProductEntity entity) throws ProductException {
        ProductEntity newEntity = new ProductEntity();
        Optional<ProductEntity> optional = repository.findById(entity.getId());
        if(optional.isEmpty()){
            throw new ProductException("Produit introuvable");
        }
        else{
            newEntity.update(entity);
            newEntity= repository.save(newEntity);
        }
        return ProductConverter.toDto(newEntity);
    }

    @Override
    public List<ProductDto> findAllProducts() {
        return repository.findAll().parallelStream()
                .map(ProductConverter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto saveProduct(ProductEntity entity) throws ProductException {
        Optional<ProductCategoryEntity> optCategory = categoryRepository.findById(entity.getId());
        if(optCategory.isEmpty()){
            throw new ProductException("Spécifier une catégorie correcte pour ce produit");
        }
        else{
            ProductEntity result = repository.save(entity);
            return ProductConverter.toDto(entity);
        }
    }
}
