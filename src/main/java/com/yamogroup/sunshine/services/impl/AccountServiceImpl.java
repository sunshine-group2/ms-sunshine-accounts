package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dao.repositories.AccountRepository;
import com.yamogroup.sunshine.dao.repositories.UserGroupRepository;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.dto.response.UserResponse;
import com.yamogroup.sunshine.enums.TypeAccount;
import com.yamogroup.sunshine.exceptions.AccountException;
import com.yamogroup.sunshine.services.AccountService;
import com.yamogroup.sunshine.services.mappers.AccountConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

  private final AccountRepository accountRepository;
  private final UserGroupRepository userGroupRepository;

  @Override
  public AccountDtoResponse saveAccount(AccountEntity entity) {
    entity.init();
    entity = accountRepository.save(entity);
    return AccountConverter.toDtoResponse(entity);
  }

  @Override
  public List<AccountDtoResponse> listAccountByType(TypeAccount accountType) {
    List<AccountEntity> accountEntities = accountRepository.findByTypeAccount(accountType);
    return accountEntities.stream()
        .map(AccountConverter::toDtoResponse)
        .collect(Collectors.toList());
  }

  @Override
  public AccountDtoResponse findAccountByIdAndType(Long accountId, TypeAccount accountType)
      throws AccountException {
    Optional<AccountEntity> optional =
        accountRepository.findByIdAndTypeAccount(accountId, accountType);
    if (!optional.isPresent()) {
      throw new AccountException("Account not found");
    }
    return AccountConverter.toDtoResponse(optional.get());
  }

  @Override
  public AccountDtoResponse updateAccount(AccountEntity entity) throws AccountException {
    AccountEntity oldVersion = findAccountById(entity.getId());
    oldVersion.update(entity);
    oldVersion = accountRepository.save(oldVersion);
    return AccountConverter.toDtoResponse(oldVersion);
  }

  @Override
  public AccountDtoResponse getAccountById(Long accountId) throws AccountException {
    Optional<AccountEntity> optional = accountRepository.findById(accountId);
    if (!optional.isPresent()) {
      throw new AccountException("Account Not Found");
    }
    AccountDtoResponse accountDto = AccountConverter.toDtoResponse(optional.get());
    return accountDto;
  }

  @Override
  public AccountDtoResponse findAccountByEmail(String email) throws AccountException {
    Optional<AccountEntity> optional = accountRepository.findByEmail(email);
    if (!optional.isPresent()) {
      throw new AccountException("Account Not Found for this email");
    }
    return AccountConverter.toDtoResponse(optional.get());
  }

  @Override
  public AccountDtoResponse findAccountByPseudo(String pseudo) throws AccountException {
    Optional<AccountEntity> optional = accountRepository.findByPseudo(pseudo);
    if (!optional.isPresent()) {
      throw new AccountException("Account Not Found for this pseudo");
    }
    return AccountConverter.toDtoResponse(optional.get());
  }

  @Override
  public List<AccountDtoResponse> findAllAccount() {
    ArrayList<AccountDtoResponse> dtos = new ArrayList<AccountDtoResponse>();
    List<AccountEntity> entities = accountRepository.findAll();
    entities.forEach((entity) -> dtos.add(AccountConverter.toDtoResponse(entity)));
    return dtos;
  }

  @Override
  public UserResponse addUser(AccountEntity accountEntity) {
    Optional<UserGroupEntity> optional =
        userGroupRepository.findByUuid(accountEntity.getUserGroup().getUuid());
    if (optional.isPresent()) {
      return AccountConverter.convertToUserResponse(
          accountRepository.save(accountEntity.toBuilder().userGroup(optional.get()).build()));
    } else {
      throw new AccountException("Trying to save user with invalid usergroup");
    }
  }

  private AccountEntity findAccountById(Long accountId) throws AccountException {
    Optional<AccountEntity> optional = accountRepository.findById(accountId);
    if (!optional.isPresent()) {
      throw new AccountException("Account to update missing");
    }
    return optional.get();
  }
}
