package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.LawGroupEntity;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dao.repositories.LawCategoryRepository;
import com.yamogroup.sunshine.dao.repositories.UserGroupRepository;
import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.exceptions.UserGroupException;
import com.yamogroup.sunshine.services.UserGroupService;
import com.yamogroup.sunshine.services.mappers.UserGroupConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserGroupServiceImpl implements UserGroupService {
    private final UserGroupRepository repository;
    private final LawCategoryRepository lawCategoryRepository;

    @Override
    public UserGroupDto saveUserGroup(UserGroupEntity entity) throws UserGroupException {
        Optional<LawGroupEntity> optLawGroupEntity = lawCategoryRepository.findById(entity.getLawGroups().getId());
        if (optLawGroupEntity.isEmpty()) {
            throw new UserGroupException("Lawgroup incorrecte");
        } else {
            entity.setLawGroups(optLawGroupEntity.get());
            entity = repository.save(entity);
        }
        return UserGroupConverter.toDto(entity);
    }
}
