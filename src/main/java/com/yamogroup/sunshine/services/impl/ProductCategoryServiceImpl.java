package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dao.repositories.ProductCategoryRepository;
import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.exceptions.ProductCategoryException;
import com.yamogroup.sunshine.services.ProductCategoryService;
import com.yamogroup.sunshine.services.mappers.ProductCategoryConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryRepository repository;

    @Override
    public ProductCategoryDto findProductCategoryById(Long categoryId)
            throws ProductCategoryException {
        Optional<ProductCategoryEntity> optional = repository.findById(categoryId);
        if (optional.isEmpty()) {
            throw new ProductCategoryException("La catégorie de produit spécifiée n'existe pas");
        }
        return ProductCategoryConverter.toDto(optional.get());
    }

    @Override
    public List<ProductCategoryDto> findAllCategories() {
        return repository.findAll().parallelStream()
                .map(ProductCategoryConverter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductCategoryDto updateProductCategory(ProductCategoryDto productCategoryDto) throws ProductCategoryException {
        Optional<ProductCategoryEntity> optional = repository.findById(productCategoryDto.getId());
        if (optional.isEmpty()) {
            throw new ProductCategoryException("Id manquant ou enregistrement manquant pour cette catégorie");
        }
        ProductCategoryEntity oldversion = optional.get();
        oldversion.update(ProductCategoryConverter.toEntity(productCategoryDto));
        oldversion = repository.save(oldversion);

        return ProductCategoryConverter.toDto(oldversion);

    }
}
