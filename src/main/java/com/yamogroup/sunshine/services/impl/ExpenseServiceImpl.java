package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import com.yamogroup.sunshine.dao.repositories.AccountRepository;
import com.yamogroup.sunshine.dao.repositories.ExpenseCategoryRepository;
import com.yamogroup.sunshine.dao.repositories.ExpenseRepository;
import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.services.ExpenseService;
import com.yamogroup.sunshine.services.mappers.ExpenseConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class ExpenseServiceImpl implements ExpenseService {
  private final ExpenseRepository repository;
  private final AccountRepository accountRepository;
  private final ExpenseCategoryRepository categoryRepository;

  @Override
  public ExpenseDto saveExpense(ExpenseEntity entity) throws ExpenseException {
    Optional<AccountEntity> optAccountEntity =
        accountRepository.findById(entity.getAuteur().getId());
    Optional<ExpenseCategoryEntity> optcategoryEntity =
        categoryRepository.findById(entity.getExpenseCategory().getId());
    if (optAccountEntity.isEmpty()) {
      throw new ExpenseException("l'auteur de la dépense est incorrecte");
    }
    if (optcategoryEntity.isEmpty()) {
      throw new ExpenseException("la catégorie de la dépense est incorrecte");
    } else {
      entity.setAuteur(optAccountEntity.get());
      entity.setExpenseCategory(optcategoryEntity.get());
      entity = repository.save(entity);
    }
    return ExpenseConverter.toDto(entity);
  }

  @Override
  public ExpenseDto updateExpense(ExpenseEntity expenseEntity) throws ExpenseException {
    Optional<ExpenseEntity> optional = repository.findById(expenseEntity.getId());
    ExpenseEntity entity = new ExpenseEntity();
    if (optional.isEmpty()) {
      throw new ExpenseException("Dépense introuvable");
    } else {
      optional.get().update(expenseEntity);
      entity = repository.save(optional.get());
    }
    return ExpenseConverter.toDto(entity);
  }
}
