package com.yamogroup.sunshine.services;

import java.util.List;

import com.yamogroup.sunshine.dao.entities.ProductEntity;
import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.exceptions.ProductException;

public interface ProductService {
    ProductDto updateProduct(ProductEntity entity) throws ProductException;
    List<ProductDto> findAllProducts();
    ProductDto saveProduct(ProductEntity toEntity) throws ProductException;
}
