package com.yamogroup.sunshine.services;

import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.exceptions.ProductCategoryException;

import java.util.List;

public interface ProductCategoryService {
  ProductCategoryDto findProductCategoryById(Long categoryId) throws ProductCategoryException;

  List<ProductCategoryDto> findAllCategories();

  ProductCategoryDto updateProductCategory(ProductCategoryDto productCategoryDto) throws ProductCategoryException;
}
