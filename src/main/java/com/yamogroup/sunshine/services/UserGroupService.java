package com.yamogroup.sunshine.services;

import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.exceptions.UserGroupException;

public interface UserGroupService {
    UserGroupDto saveUserGroup(UserGroupEntity entity) throws UserGroupException;
}
