package com.yamogroup.sunshine.services.mappers;
import com.yamogroup.sunshine.dao.entities.*;
import com.yamogroup.sunshine.dto.UserGroupDto;

public class UserGroupConverter {
    public static UserGroupEntity toEntity(UserGroupDto dto) {
        UserGroupEntity entity = new UserGroupEntity();
        LawGroupEntity lawGroupEntity = new LawGroupEntity();
        lawGroupEntity.setId(dto.getLawgroupsId());
        entity.setId(dto.getId());
        entity.setLibelle(dto.getLibelle());
        entity.setLawGroups(lawGroupEntity);
        return entity;
    }

    public static UserGroupDto toDto(UserGroupEntity entity) {
        UserGroupDto dto = new UserGroupDto();
        dto.setId(entity.getId());
        dto.setLibelle(entity.getLibelle());
        dto.setLawgroupsId(entity.getLawGroups().getId());
        return dto;
    }
}
