package com.yamogroup.sunshine.services.mappers;

import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dto.ExpenseCategoryDto;

public class ExpenseCategoryConverter {
  public static ExpenseCategoryEntity toEntity(ExpenseCategoryDto dto) {
    if (dto == null) {
      return null;
    }
    ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
    entity.setId(dto.getId());
    entity.setPaiement_fixe(dto.getPaiement_fixe());
    entity.setType(dto.getType());
    entity.setUuid(dto.getUuid());
    entity.setCreatedAt(dto.getCreatedAt());
    entity.setUpdatedAt(dto.getUpdatedAt());
    return entity;
  }

  public static ExpenseCategoryDto toDto(ExpenseCategoryEntity entity) {
    if (entity == null) {
      return null;
    }
    ExpenseCategoryDto dto = new ExpenseCategoryDto();
    dto.setCreatedAt(entity.getCreatedAt());
    dto.setId(entity.getId());
    dto.setPaiement_fixe(entity.getPaiement_fixe());
    dto.setType(entity.getType());
    dto.setUuid(entity.getUuid());
    dto.setUpdatedAt(entity.getUpdatedAt());
    return dto;
  }
}
