package com.yamogroup.sunshine.services.mappers;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ProductEntity;
import com.yamogroup.sunshine.dao.entities.TransactionEntity;
import com.yamogroup.sunshine.dao.entities.UnitEntity;
import com.yamogroup.sunshine.dto.ProductDto;

import java.util.HashSet;
import java.util.Set;

public class ProductConverter {
    public static ProductEntity toEntity(ProductDto productDto) {
        if (productDto == null) {
            return null;
        }
        ProductEntity entity = new ProductEntity();
        ProductCategoryEntity categoryEntity = new ProductCategoryEntity();
        UnitEntity unitEntity = new UnitEntity();
        Set<TransactionEntity> transactions = new HashSet<TransactionEntity>();
        categoryEntity.setId(productDto.getIdCategory());
        TransactionEntity transactionEntity = new TransactionEntity();
        entity.setId(productDto.getId());
        entity.setDesignation(productDto.getDesignation());
        entity.setPrix_achat(productDto.getPrix_achat());
        entity.setPrix_vente(productDto.getPrix_vente());
        entity.setCode_barre(productDto.getCode_barre());
        entity.setProductCategory(categoryEntity);
        entity.setUnitEntity(unitEntity);
        for (Long idTransaction : productDto.getTransaction()) {
            transactionEntity.setId(idTransaction);
            transactions.add(transactionEntity);
        }
        entity.setTransaction(transactions);
        return entity;
    }

    public static ProductDto toDto(ProductEntity entity) {
        ProductDto productDto = new ProductDto();
        if (entity == null) {
            return null;
        }
        Set<Long> transactions = new HashSet<Long>();
        productDto.setId(entity.getId());
        productDto.setDesignation(entity.getDesignation());
        productDto.setPrix_achat(entity.getPrix_achat());
        productDto.setPrix_vente(entity.getPrix_vente());
        productDto.setCode_barre(entity.getCode_barre());
        productDto.setIdCategory(entity.getProductCategory().getId());
        productDto.setCreatedAt(entity.getCreatedAt());
        productDto.setIdUnit(entity.getUnitEntity().getId());
        for (TransactionEntity transaction : entity.getTransaction()) {
            transactions.add(transaction.getId());
        }
        productDto.setTransaction(transactions);
        productDto.setSeuil(entity.getSeuil());
        productDto.setQuantite(entity.getQuantite());
        return productDto;
    }
}
