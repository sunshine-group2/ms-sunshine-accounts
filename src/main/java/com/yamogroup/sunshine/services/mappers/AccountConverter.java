package com.yamogroup.sunshine.services.mappers;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dto.request.AccountDtoRequest;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.dto.response.UserResponse;

public class AccountConverter {
  public static AccountDtoResponse toDtoResponse(AccountEntity entity) {
    if (entity == null) {
      return null;
    }
    AccountDtoResponse dto = new AccountDtoResponse();
    dto.setId(entity.getId());
    dto.setEmail(entity.getEmail());
    dto.setPseudo(entity.getPseudo());
    return dto;
  }

  public static AccountEntity toEntity(AccountDtoRequest request) {
    if (request == null) {
      return null;
    }
    AccountEntity entity = new AccountEntity();
    UserGroupEntity group = new UserGroupEntity();
    group.setId(request.getUserGroupId());
    entity.setId(request.getId());
    entity.setNom(request.getNom());
    entity.setPrenom(request.getPrenom());
    entity.setPassword(request.getPassword());
    entity.setPseudo(request.getPseudo());
    entity.setSexe(request.getSexe());
    entity.setStatut(request.getStatut());
    entity.setTel(request.getTel());
    entity.setDate_naissance(request.getDate_naissance());
    entity.setEmail(request.getEmail());
    entity.setAdresse(request.getAdresse());
    entity.setTypeAccount(request.getTypeAccount());
    entity.setUserGroup(group);
    return entity;
  }

  public static UserResponse convertToUserResponse(AccountEntity accountEntity) {
    return UserResponse.builder().uuid(accountEntity.getUuid()).build();
  }
}
