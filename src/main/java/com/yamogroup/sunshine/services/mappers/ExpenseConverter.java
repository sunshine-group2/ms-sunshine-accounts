package com.yamogroup.sunshine.services.mappers;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import com.yamogroup.sunshine.dto.ExpenseDto;

public class ExpenseConverter {
  public static ExpenseEntity toEntity(ExpenseDto dto) {
    ExpenseEntity entity = new ExpenseEntity();
    AccountEntity accountEntity = new AccountEntity();
    ExpenseCategoryEntity expenseCategoryEntity = new ExpenseCategoryEntity();
    accountEntity.setId(dto.getAccountId());
    expenseCategoryEntity.setId(dto.getCategoryId());
    entity.setId(dto.getId());
    entity.setAuteur(accountEntity);
    entity.setExpenseCategory(expenseCategoryEntity);
    entity.setFrais(dto.getFrais());
    return entity;
  }

  public static ExpenseDto toDto(ExpenseEntity entity) {
    ExpenseDto dto = new ExpenseDto();
    dto.setId(entity.getId());
    dto.setFrais(entity.getFrais());
    dto.setAccountId(entity.getAuteur().getId());
    dto.setCategoryId(entity.getExpenseCategory().getId());
    return dto;
  }
}
