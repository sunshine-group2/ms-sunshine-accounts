package com.yamogroup.sunshine.services.mappers;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dto.ProductCategoryDto;

public class ProductCategoryConverter {
    public static ProductCategoryEntity toEntity(ProductCategoryDto dto) {
        if (dto == null) {
            return null;
        }
        ProductCategoryEntity entity = new ProductCategoryEntity();
        entity.setId(dto.getId());
        entity.setPaiement_fixe(dto.getPaiement_fixe());
        entity.setType(dto.getType());
        entity.setUuid(dto.getUuid());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
        return entity;
    }

    public static ProductCategoryDto toDto(ProductCategoryEntity entity) {
        if (entity == null) {
            return null;
        }
        ProductCategoryDto dto = new ProductCategoryDto();
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setId(entity.getId());
        dto.setPaiement_fixe(entity.getPaiement_fixe());
        dto.setType(entity.getType());
        dto.setUuid(entity.getUuid());
        dto.setUpdatedAt(entity.getUpdatedAt());
        return dto;
    }
}
