package com.yamogroup.sunshine.exceptions;

public class AccountException extends RuntimeException {
  public AccountException(String message) {
    super(message);
  }
}
