package com.yamogroup.sunshine.exceptions;

public class ProductCategoryException extends Exception {
  public ProductCategoryException(String message) {
    super(message);
  }
}
