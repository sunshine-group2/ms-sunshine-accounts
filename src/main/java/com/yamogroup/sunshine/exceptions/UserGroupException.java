package com.yamogroup.sunshine.exceptions;

public class UserGroupException extends Exception{
    public UserGroupException(String message) {
        super(message);
    }
}
