package com.yamogroup.sunshine.exceptions;

public class ExpenseException extends Exception {
  public ExpenseException(String message) {
    super(message);
  }
}
