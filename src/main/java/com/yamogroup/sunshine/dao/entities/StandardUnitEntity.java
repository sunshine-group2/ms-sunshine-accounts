package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "unite_standard")
public class StandardUnitEntity extends UnitEntity {
  @Column(nullable = false)
  private String type_operation;

  @Column(nullable = false)
  private String valeur_operation;
}
