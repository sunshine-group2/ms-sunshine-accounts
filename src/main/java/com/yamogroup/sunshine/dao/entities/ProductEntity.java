package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "produit")
public class ProductEntity extends BasicEntity {
    @Column(nullable = false)
    private String designation;
    private int quantite;
    private double prix_achat;
    private double prix_vente;
    @Column(nullable = false, unique = true)
    private int seuil;
    private String code_barre;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, unique = true, referencedColumnName = "id", name = "categorie_produit")
    private ProductCategoryEntity productCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, referencedColumnName = "id", name = "unite_de_base")
    private UnitEntity unitEntity;

    @ManyToMany(mappedBy = "products")
    private Set<TransactionEntity> transaction;


    @Override
    public String toString() {
        return "ProductEntity{" +
                "designation='" + designation + '\'' +
                ", quantite=" + quantite +
                ", prix_achat=" + prix_achat +
                ", prix_vente=" + prix_vente +
                ", seuil=" + seuil +
                ", code_barre='" + code_barre + '\'' +
                '}';
    }

    public void update(ProductEntity productEntity) {
        this.setId(productEntity.getId());
        this.setQuantite(productEntity.getQuantite());
        this.setProductCategory(productEntity.getProductCategory());
        this.setPrix_vente(productEntity.getPrix_vente());
        this.setPrix_achat(productEntity.getPrix_achat());
        this.setCode_barre(productEntity.getCode_barre());
        this.setDesignation(productEntity.getDesignation());
        this.setTransaction(productEntity.getTransaction());
        this.setCreatedAt(productEntity.getCreatedAt());
        this.setUpdatedAt(productEntity.getUpdatedAt());
        this.setSeuil(productEntity.getSeuil());
        this.setUnitEntity(productEntity.getUnitEntity());
    }
}
