package com.yamogroup.sunshine.dao.entities;

import com.yamogroup.sunshine.enums.TypeTransaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="transaction")
public class TransactionEntity extends BasicEntity{
    @Column(nullable=false)
    @Enumerated(EnumType.STRING)
    private TypeTransaction typeTransaction;
    @ManyToMany
    @JoinTable(
            name="transaction_produit",
            joinColumns = @JoinColumn(name="transaction_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name="product_id", referencedColumnName = "id")
    )
    private Set<ProductEntity> products;
}
