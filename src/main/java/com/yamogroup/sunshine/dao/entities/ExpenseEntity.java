package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "depense")
public class ExpenseEntity extends BasicEntity {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(nullable = false, referencedColumnName = "id", name = "auteur")
  private AccountEntity auteur;

  @Column(nullable = false)
  private double frais;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(nullable = false, referencedColumnName = "id", name = "categorieDepense")
  private ExpenseCategoryEntity expenseCategory;

  public void update(ExpenseEntity entity) {
    this.setId(entity.getId());
    this.setExpenseCategory(entity.getExpenseCategory());
    this.setAuteur(entity.getAuteur());
    this.setFrais(entity.getFrais());
  }
}
