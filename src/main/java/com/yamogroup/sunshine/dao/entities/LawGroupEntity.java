package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "groupe_de_lois")
public class LawGroupEntity extends BasicEntity {
  @Column(nullable = false, unique = true)
  private String libelle;

  @ManyToMany(mappedBy = "groups")
  private Collection<LawEntity> laws;
}
