package com.yamogroup.sunshine.dao.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "categorie_depense")
public class ExpenseCategoryEntity extends BasicEntity {
  @Column(nullable = false, unique = true)
  private String type;

  @Column(nullable = false)
  private double paiement_fixe;

    @Override
    public String toString() {
        return "ExpenseCategoryEntity{" +
                "type='" + type + '\'' +
                ", paiement_fixe=" + paiement_fixe +
                ", createdAt=" + createdAt +
                '}';
    }

  public void update(ExpenseCategoryEntity entity) {
    this.setType(entity.getType());
    this.setPaiement_fixe(entity.getPaiement_fixe());
  }
}
