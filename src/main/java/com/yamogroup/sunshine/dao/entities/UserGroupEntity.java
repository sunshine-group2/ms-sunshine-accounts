package com.yamogroup.sunshine.dao.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "groupe_utilisateur")
public class UserGroupEntity extends BasicEntity {
  @Column(nullable = false, unique = true)
  private String libelle;

  @OneToMany(mappedBy = "userGroup")
  private Collection<LawEntity> laws = Collections.emptyList();

  @OneToOne
  @JoinColumn(name = "law_group", referencedColumnName = "id")
  private LawGroupEntity lawGroups;

  @Builder(toBuilder = true)
  public UserGroupEntity(
      Long id,
      long uuid,
      Date createdAt,
      Date updatedAt,
      String libelle,
      Collection<LawEntity> laws,
      LawGroupEntity lawGroups) {
    super(id, uuid, createdAt, updatedAt);
    this.libelle = libelle;
    this.laws = laws == null ? Collections.emptyList() : laws;
    this.lawGroups = lawGroups;
  }
}
