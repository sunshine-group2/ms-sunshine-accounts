package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "droit")
public class LawEntity extends BasicEntity {
  @Column(nullable = false, unique = true)
  private String nom;

  @ManyToMany()
  @JoinTable(
      name = "lawgroup_law",
      joinColumns = @JoinColumn(name = "law_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "lawGroup_id", referencedColumnName = "id"))
  private Collection<LawGroupEntity> groups;

  @ManyToOne
  @JoinColumn(name = "groupe_utilisateur", referencedColumnName = "id")
  private UserGroupEntity userGroup;
}
