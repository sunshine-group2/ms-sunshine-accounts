package com.yamogroup.sunshine.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "categorie_produit")
public class ProductCategoryEntity extends BasicEntity {
    @Column(nullable = false, unique = true)
    private String type;

    @Column(nullable = false)
    private double paiement_fixe;


    @Override
    public String toString() {
        return "ProductCategoryEntity{" +
                "type='" + type + '\'' +
                ", paiement_fixe=" + paiement_fixe +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public void update(ProductCategoryEntity entity) {
        this.setType(entity.getType());
        this.setPaiement_fixe(entity.getPaiement_fixe());
    }

}
