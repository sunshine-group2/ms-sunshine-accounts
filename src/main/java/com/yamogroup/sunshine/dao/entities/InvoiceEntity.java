package com.yamogroup.sunshine.dao.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "facture")
public class InvoiceEntity extends BasicEntity {

  @OneToMany(mappedBy = "invoice")
  private Collection<AccountEntity> comptes;

}
