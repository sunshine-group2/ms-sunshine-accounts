package com.yamogroup.sunshine.dao.entities;

import com.yamogroup.sunshine.enums.Statut;
import com.yamogroup.sunshine.enums.TypeAccount;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "compte")
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity extends BasicEntity {
  @Column(nullable = false)
  private String nom;

  @Column(nullable = false)
  private String prenom;

  @Column(unique = true)
  private String email;

  private String adresse;
  private String sexe;
  private Date date_naissance;

  @Column(unique = true)
  private String pseudo;

  private String password;
  private String fonction;
  private String tel;

  @Enumerated(EnumType.STRING)
  private TypeAccount typeAccount;

  public void init() {
    this.statut = Statut.ACTIF;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(referencedColumnName = "id", name = "facture")
  private InvoiceEntity invoice;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinColumn(referencedColumnName = "id", name = "groupe_utilisateur")
  private UserGroupEntity userGroup;

  @Enumerated(EnumType.STRING)
  public Statut statut;

  public void update(AccountEntity entity) {
    this.setNom(entity.getNom());
    this.setPrenom(entity.getPrenom());
    this.setDate_naissance(entity.getDate_naissance());
    this.setEmail(entity.getEmail());
    this.setAdresse(entity.getAdresse());
    this.setFonction(entity.getFonction());
    this.setTel(entity.getTel());
    this.setUserGroup(entity.getUserGroup());
    this.setPseudo(entity.getPseudo());
    this.setPassword(entity.getPassword());
    this.setTypeAccount(entity.getTypeAccount());
  }
}
