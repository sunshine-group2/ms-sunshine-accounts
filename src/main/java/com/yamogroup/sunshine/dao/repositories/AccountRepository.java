package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.enums.TypeAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

  List<AccountEntity> findByTypeAccount(TypeAccount typeAccount);

  Optional<AccountEntity> findByIdAndTypeAccount(Long id, TypeAccount typeAccount);

  Optional<AccountEntity> findByEmail(String email);

  Optional<AccountEntity> findByPseudo(String pseudo);
}
