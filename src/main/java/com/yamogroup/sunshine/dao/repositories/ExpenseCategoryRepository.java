package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpenseCategoryRepository extends JpaRepository<ExpenseCategoryEntity, Long> {}
