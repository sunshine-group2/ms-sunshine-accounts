package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.LawEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LawRepository extends JpaRepository<LawEntity, Long> {}
