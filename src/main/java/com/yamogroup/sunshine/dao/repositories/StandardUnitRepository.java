package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.StandardUnitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StandardUnitRepository extends JpaRepository<StandardUnitEntity, Long> {}
