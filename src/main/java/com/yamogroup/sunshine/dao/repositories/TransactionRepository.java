package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {}
