package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserGroupRepository extends JpaRepository<UserGroupEntity, Long> {

  Optional<UserGroupEntity> findByUuid(long uuid);
}
