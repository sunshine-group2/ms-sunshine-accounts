package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryRepository extends JpaRepository<ProductCategoryEntity, Long> {}
