package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.InvoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<InvoiceEntity, Long> {}
