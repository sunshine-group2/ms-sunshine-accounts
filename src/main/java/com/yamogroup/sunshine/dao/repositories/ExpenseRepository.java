package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpenseRepository extends JpaRepository<ExpenseEntity, Long> {}
