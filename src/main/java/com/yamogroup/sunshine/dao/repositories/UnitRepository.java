package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.UnitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitRepository extends JpaRepository<UnitEntity, Long> {}
