package com.yamogroup.sunshine.dao.repositories;

import com.yamogroup.sunshine.dao.entities.LawGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LawCategoryRepository extends JpaRepository<LawGroupEntity, Long> {}
