package com.yamogroup.sunshine.config;

import com.yamogroup.sunshine.dto.ErrorResponse;
import com.yamogroup.sunshine.exceptions.AccountException;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.exceptions.ProductException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException exception, WebRequest request) {
    return buildErrorResponse(
        exception.getBindingResult().getFieldError().getDefaultMessage(),
        request,
        HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(AccountException.class)
  public ResponseEntity<ErrorResponse> handleAccountException(
      AccountException exception, WebRequest request) {
    return buildErrorResponse(exception.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<ErrorResponse> handleHttpRequestMethodNotSupportedException(
      HttpRequestMethodNotSupportedException exception, WebRequest request) {
    String message = "la méthode n'est pas supportée";
    return buildErrorResponse(message, request, HttpStatus.METHOD_NOT_ALLOWED);
  }

  @ExceptionHandler(ExpenseException.class)
  public ResponseEntity<ErrorResponse> handleExpenseException(
      ExpenseException exception, WebRequest request) {
    return buildErrorResponse(exception.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ErrorResponse> handleIllegalArgumentException(
      IllegalArgumentException exception, WebRequest request) {
    String message = "Les arguments de la méthode ne sont pas respectés";
    return buildErrorResponse(message, request, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(NoSuchElementException.class)
  public ResponseEntity<ErrorResponse> handleNoSuchElementException(
      NoSuchElementException exception, WebRequest request) {
    return buildErrorResponse(exception.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ProductException.class)
  public ResponseEntity<ErrorResponse> handleProductException(
          ProductException exception, WebRequest request){
    return buildErrorResponse(exception.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private ResponseEntity<ErrorResponse> buildErrorResponse(
      String message, WebRequest request, HttpStatus status) {
    String path = request.getDescription(false).substring(4);
    ErrorResponse response =
        ErrorResponse.builder().message(message).date(new Date()).path(path).build();
    return ResponseEntity.status(status).body(response);
  }
}
