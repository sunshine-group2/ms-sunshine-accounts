package com.yamogroup.sunshine.enums;

public enum Statut {
  ACTIF,
  ARCHIVE,
  INACTIF;
}
