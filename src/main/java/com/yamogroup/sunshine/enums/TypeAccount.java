package com.yamogroup.sunshine.enums;

public enum TypeAccount {
  PROVIDER,
  USER,
  CUSTOMER;
}
