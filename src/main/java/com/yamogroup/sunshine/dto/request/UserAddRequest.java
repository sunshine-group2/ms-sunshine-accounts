package com.yamogroup.sunshine.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.enums.Statut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAddRequest {

  @NotEmpty(message = "username is required")
  private String username;

  @NotEmpty(message = "email is required")
  @Email(message = "email is invalid")
  private String email;

  @NotEmpty(message = "password is required")
  private String password;

  private String phoneNumber;

  @NotNull(message = "userGroupId is required")
  private Long userGroupId;

  @NotEmpty(message = "status is required")
  private Statut status;

  @NotEmpty(message = "gender is required")
  private String gender;

  @NotEmpty(message = "firstname is required")
  private String firstname;

  @NotEmpty(message = "lastname is required")
  private String lastname;

  public AccountEntity toEntity() {
    return AccountEntity.builder()
        .pseudo(this.username)
        .prenom(this.firstname)
        .nom(this.lastname)
        .email(this.email)
        .password(this.password)
        .tel(this.phoneNumber)
        .statut(this.status)
        .sexe(this.gender)
        .userGroup(UserGroupEntity.builder().uuid(userGroupId).build())
        .build();
  }
}
