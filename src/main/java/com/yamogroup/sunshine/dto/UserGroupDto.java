package com.yamogroup.sunshine.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserGroupDto implements Serializable {
    private Long id;
    private long uuid;
    private Date createdAt;
    private Date updatedAt;
    @NotEmpty(message = "Le paramètre libelle est obligatoire")
    private String libelle;
    @NotEmpty(message = "Paramètre obligatoire")
    private Long lawgroupsId;
}
