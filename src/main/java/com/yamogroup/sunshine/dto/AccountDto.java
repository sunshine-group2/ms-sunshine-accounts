package com.yamogroup.sunshine.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.enums.Statut;
import com.yamogroup.sunshine.enums.TypeAccount;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDto implements Serializable {
  @NotNull(
      message = "Le paramètre Id est obligatoire",
      groups = {AccountUpdate.class})
  private Long id;

  private long uuid;
  private Date createdAt;
  private Date updatedAt;

  @NotEmpty(message = "Le paramètre nom est obligatoire")
  private String nom;

  @NotEmpty(message = "Le paramètre prenom est obligatoire")
  private String prenom;

  @NotEmpty(message = "Le paramètre email est obligatoire")
  @Email(message = "Votre email est invalide")
  private String email;

  private String tel;
  private String adresse;
  private String sexe;
  private Date date_naissance;

  @NotEmpty(message = "Le paramètre pseudo est obligatoire")
  private String pseudo;

  @NotEmpty(message = "Le paramètre password est obligatoire")
  private String password;

  private String fonction;
  private TypeAccount typeAccount;
  private UserGroupEntity userGroup;
  private Statut statut;
  private String confirmation;
}
