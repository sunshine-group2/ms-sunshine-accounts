package com.yamogroup.sunshine.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ErrorResponse implements Serializable {
  private String message;
  private String path;
  private Date date;
}
