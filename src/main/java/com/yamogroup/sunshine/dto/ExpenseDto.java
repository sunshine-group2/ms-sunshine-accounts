package com.yamogroup.sunshine.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpenseDto implements Serializable {

    private Long id;
    @NotNull(message="l'id de l'auteur n'a pas été renseigné")
    private Long accountId;
    @NotNull(message="les frais sont inexistants")
    private double frais;
    @NotNull(message="l'id de la  catégorie de dépense n'a pas été spécifiée")
    private Long categoryId;
}
