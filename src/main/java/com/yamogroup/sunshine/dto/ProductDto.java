package com.yamogroup.sunshine.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

import java.util.Date;
import java.util.Set;

@RequiredArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDto {
    private Long id;
    @NotNull(message = "indiquer la désignation du produit")
    private String designation;
    private int quantite;
    private double prix_achat;
    private double prix_vente;
    private Date createdAt;
    @NotNull(message = "indiquer le seuil de stock")
    private int seuil;
    private String code_barre;
    private Long idCategory;
    private Long idUnit;
    private Set<Long> transaction;
}
