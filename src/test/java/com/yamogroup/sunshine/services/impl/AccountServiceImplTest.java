package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.repositories.AccountRepository;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.exceptions.AccountException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {
  @Mock private AccountRepository repository;
  @InjectMocks private AccountServiceImpl objectToTest;

  @Test
  public void testSuccessful_updateAccount() {
    Long accountId = 1L;
    final AccountEntity entity = new AccountEntity();
    entity.setNom("Ambara");
    entity.setPrenom("Mevongo");
    entity.setPseudo("ambMevongo");
    entity.setEmail("ambaramevongo@gmail.com");
    entity.setId(accountId);
    entity.setPassword("BLEM1405");
    entity.init();
    AccountDtoResponse result = new AccountDtoResponse();
    result.setId(2L);
    result.setEmail("johnstone@gmail.com");
    result.setPseudo("Beric237");
    when(repository.findById(accountId)).thenReturn(Optional.of(entity));

    result = objectToTest.getAccountById(accountId);
    final AccountDtoResponse dto = result;
    assertAll(
        "Test de mise à jour complète",
        () -> assertEquals(accountId, dto.getId(), "Même id attendu"),
        () -> assertEquals(entity.getEmail(), dto.getEmail(), "mails identiques"),
        () -> assertEquals(entity.getPseudo(), dto.getPseudo(), "pseudo identiques"));
  }

  @Test
  public void testThrowException_updateAccount() {
    Long accountId = 1L;
    String errorMessage = "Account to update missing";
    AccountEntity entity = new AccountEntity();
    entity.setId(accountId);
    when(repository.findById(entity.getId())).thenReturn(Optional.empty());

    AccountException result =
        assertThrows(AccountException.class, () -> objectToTest.updateAccount(entity));
    assertEquals(errorMessage, result.getMessage());
  }

  @Test
  public void testSuccessful_getAccountById() {
    Long accountId = 1L;
    AccountEntity entity = new AccountEntity();
    entity.setId(accountId);
    when(repository.findById(accountId)).thenReturn(Optional.of(entity));

    AccountDtoResponse result = objectToTest.getAccountById(accountId);
    assertEquals(accountId, result.getId());
  }

  @Test
  public void testThrowException_getAccountById() {
    Long accountId = 1L;
    String errorMessage = "Account Not Found";
    when(repository.findById(accountId)).thenReturn(Optional.empty());

    AccountException result =
        assertThrows(AccountException.class, () -> objectToTest.getAccountById(accountId));
    assertEquals(errorMessage, result.getMessage());
  }

  @Test
  public void testSuccessful_findAccountByEmail() {
    Long accountId = 1L;
    String email = "ambaramevongo@gmail.com";
    final AccountEntity entity = new AccountEntity();
    entity.setNom("Ambara");
    entity.setPrenom("Mevongo");
    entity.setPseudo("ambMevongo");
    entity.setEmail(email);
    entity.setId(accountId);
    entity.setPassword("BLEM1405");
    entity.init();
    when(repository.findByEmail(entity.getEmail())).thenReturn(Optional.of(entity));

    AccountDtoResponse result = objectToTest.findAccountByEmail(entity.getEmail());
    final AccountDtoResponse dto = result;
    assertAll(
        "recherche de compte à partir d'email",
        () -> assertEquals(accountId, dto.getId(), "Même id attendu"),
        () -> assertEquals(email, dto.getEmail(), "mails identiques"),
        () -> assertEquals(entity.getPseudo(), dto.getPseudo(), "pseudo identiques"));
  }

  @Test
  public void testThrowException_findAccountByEmail() {
    String email = "johnsnow@gmail.com";
    String errorMessage = "Account Not Found for this email";
    when(repository.findByEmail(email)).thenReturn(Optional.empty());

    AccountException result =
        assertThrows(AccountException.class, () -> objectToTest.findAccountByEmail(email));
    assertEquals(errorMessage, result.getMessage());
  }

  @Test
  public void testSuccessful_findAccountByPseudo() {
    String pseudo = "leviathan";
    final AccountEntity entity = new AccountEntity();
    entity.setNom("Ambara");
    entity.setPrenom("Mevongo");
    entity.setPseudo(pseudo);
    entity.setEmail("email@gmail.com");
    entity.setId(1L);
    entity.setPassword("BLEM1405");
    entity.init();
    when(repository.findByPseudo(pseudo)).thenReturn(Optional.of(entity));

    AccountDtoResponse result = objectToTest.findAccountByPseudo(entity.getPseudo());
    final AccountDtoResponse dto = result;
    assertAll(
        "recherche de compte à partir d'email",
        () -> assertEquals(entity.getId(), dto.getId(), "Même id attendu"),
        () -> assertEquals(entity.getEmail(), dto.getEmail(), "mails identiques"),
        () -> assertEquals(pseudo, dto.getPseudo(), "pseudo identiques"));
  }

  @Test
  public void testThrowException_findAccountByPseudo() {
    String pseudo = "snowjohn";
    String errorMessage = "Account Not Found for this pseudo";
    when(repository.findByPseudo(pseudo)).thenReturn(Optional.empty());

    AccountException result =
        assertThrows(AccountException.class, () -> objectToTest.findAccountByPseudo(pseudo));
    assertEquals(errorMessage, result.getMessage());
  }
}
