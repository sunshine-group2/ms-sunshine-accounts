package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import com.yamogroup.sunshine.dao.repositories.AccountRepository;
import com.yamogroup.sunshine.dao.repositories.ExpenseCategoryRepository;
import com.yamogroup.sunshine.dao.repositories.ExpenseRepository;
import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseServiceImplTest {
  @Mock private ExpenseRepository repository;
  @Mock private AccountRepository accountRepository;
  @Mock private ExpenseCategoryRepository categoryRepository;
  @InjectMocks private ExpenseServiceImpl objectToTest;

  @Test
  public void testSuccessfull_saveExpense() throws Exception {
    // préparation
    Long id = 1L;
    Double frais = 2500.0;
    Long accountId = 1L;
    Long categoryId = 1L;
    String nom = "snow";
    String prenom = "john";
    String email = "johnsnow@gmail.com";
    String pseudo = "johnsnow";
    String type = "gels";
    Double paiement_fixe = 2500.0;
    AccountEntity accountEntity = new AccountEntity();
    ExpenseCategoryEntity categoryEntity = new ExpenseCategoryEntity();
    ExpenseEntity entity = new ExpenseEntity();
    accountEntity.setNom(nom);
    accountEntity.setPrenom(prenom);
    accountEntity.setEmail(email);
    accountEntity.setId(accountId);
    categoryEntity.setId(categoryId);
    categoryEntity.setType(type);
    categoryEntity.setPaiement_fixe(paiement_fixe);
    entity.setFrais(frais);
    entity.setExpenseCategory(categoryEntity);
    entity.setAuteur(accountEntity);
    entity.setId(id);

    when(accountRepository.findById(accountId)).thenReturn(Optional.of(accountEntity));
    when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categoryEntity));
    when(repository.save(entity)).thenReturn(entity);
    // Exécution
    final ExpenseDto dto = objectToTest.saveExpense(entity);
    // Vérification
    assertAll(
        () -> assertEquals(id, dto.getId(), "id non similaires"),
        () -> assertEquals(accountId, dto.getAccountId(), "les comptes sont différents"),
        () -> assertEquals(categoryId, dto.getCategoryId(), "les catégories sont différents"),
        () -> assertEquals(frais, dto.getFrais(), "les frais sont différents"));
  }

  @Test
  public void testThrowException_saveExpense() {
    Long accountId = 1L;
    Long expenseId = 1L;
    Long categoryId = 1L;
    AccountEntity accountEntity = new AccountEntity();
    ExpenseCategoryEntity categoryEntity = new ExpenseCategoryEntity();
    categoryEntity.setId(categoryId);
    accountEntity.setId(accountId);
    ExpenseEntity entity = new ExpenseEntity();
    entity.setId(expenseId);
    entity.setAuteur(accountEntity);
    entity.setExpenseCategory(categoryEntity);
    String errorMessage = "l'auteur de la dépense est incorrecte";
    when(accountRepository.findById(accountId)).thenReturn(Optional.empty());
    when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());
    ExpenseException result =
        assertThrows(ExpenseException.class, () -> objectToTest.saveExpense(entity));
    assertEquals(errorMessage, result.getMessage());
  }

  @Test
  public void testSuccessfull_updateExpense() throws Exception {
    // préparation
    Long id = 1L;
    Double oldFrais = 2500.0;
    Long oldAccountId = 1L;
    Long oldCategoryId = 1L;
    String oldNom = "snow";
    String oldPrenom = "john";
    String oldEmail = "johnsnow@gmail.com";
    String oldPseudo = "johnsnow";
    String oldType = "gels";
    Double oldPaiement_fixe = 2500.0;
    AccountEntity oldAccountEntity = new AccountEntity();
    ExpenseCategoryEntity oldCategoryEntity = new ExpenseCategoryEntity();
    oldAccountEntity.setId(oldAccountId);
    oldAccountEntity.setEmail(oldEmail);
    oldAccountEntity.setNom(oldNom);
    oldAccountEntity.setPrenom(oldPrenom);
    oldAccountEntity.setPseudo(oldPseudo);
    oldCategoryEntity.setId(oldCategoryId);
    oldCategoryEntity.setPaiement_fixe(oldPaiement_fixe);
    oldCategoryEntity.setType(oldType);
    ExpenseEntity oldEntity = new ExpenseEntity();
    oldEntity.setFrais(oldFrais);
    oldEntity.setExpenseCategory(oldCategoryEntity);
    oldEntity.setAuteur(oldAccountEntity);
    oldEntity.setId(id);

    Double newFrais = 2600.0;
    Long newAccountId = 2L;
    Long newCategoryId = 2L;
    String newNom = "James";
    String newPrenom = "Bryce";
    String newEmail = "jamesbryce@gmail.com";
    String newPseudo = "jamesbryce";
    String newType = "imprimables";
    Double newPaiement_fixe = 2525.0;
    AccountEntity newAccountEntity = new AccountEntity();
    ExpenseCategoryEntity newCategoryEntity = new ExpenseCategoryEntity();
    newAccountEntity.setId(newAccountId);
    newAccountEntity.setEmail(newEmail);
    newAccountEntity.setNom(newNom);
    newAccountEntity.setPrenom(newPrenom);
    newAccountEntity.setPseudo(newPseudo);
    newCategoryEntity.setId(newCategoryId);
    newCategoryEntity.setPaiement_fixe(newPaiement_fixe);
    newCategoryEntity.setType(newType);
    ExpenseEntity newEntity = new ExpenseEntity();
    newEntity.setFrais(newFrais);
    newEntity.setExpenseCategory(newCategoryEntity);
    newEntity.setAuteur(newAccountEntity);
    newEntity.setId(id);
    when(repository.findById(id)).thenReturn(Optional.of(oldEntity));
    when(repository.save(Mockito.any(ExpenseEntity.class))).thenReturn(newEntity);
    // Exécution
    final ExpenseDto dto = objectToTest.updateExpense(newEntity);
    // Vérification
    assertAll(
        () -> assertEquals(id, dto.getId(), "id différents"),
        () -> assertEquals(newAccountId, dto.getAccountId(), "auteurs différents"),
        () -> assertEquals(newCategoryId, dto.getCategoryId(), "catégories différentes"),
        () -> assertEquals(newFrais, dto.getFrais(), "frais différents"));
  }

  @Test
  public void testThrowException_updateExpense() {
    ExpenseEntity entity = new ExpenseEntity();
    String errorMessage = "Dépense introuvable";
    Long expenseId = 1L;
    entity.setId(expenseId);
    when(repository.findById(expenseId)).thenReturn(Optional.empty());
    ExpenseException result =
        assertThrows(ExpenseException.class, () -> objectToTest.updateExpense(entity));
    assertEquals(errorMessage, result.getMessage());
  }
}
