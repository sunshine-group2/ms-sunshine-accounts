package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dao.entities.ProductEntity;
import com.yamogroup.sunshine.dao.entities.TransactionEntity;
import com.yamogroup.sunshine.dao.entities.UnitEntity;
import com.yamogroup.sunshine.dao.repositories.ProductCategoryRepository;
import com.yamogroup.sunshine.dao.repositories.ProductRepository;
import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.exceptions.ProductException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {
    @Mock
    private ProductRepository repository;
    @Mock
    private ProductCategoryRepository categoryRepository;
    @InjectMocks
    private ProductServiceImpl objectToTest;

    @Test
    public void testSuccessfulUpdateProduct() throws Exception {
        //preparation
        ProductEntity oldEntity = new ProductEntity();
        Long id = 1L;
        Long oldIdCategory = 1L;
        Double oldPrixVente = 2000.0;
        Double oldPrixAchat = 2500.0;
        int oldQuantite = 500;
        int oldSeuil = 5;
        String oldDesignation = "moby bébé";
        Long oldIdUnit = 1L;
        String code_barre = "empty";
        ProductCategoryEntity oldCategory = new ProductCategoryEntity();
        UnitEntity oldUnit = new UnitEntity();
        Set<TransactionEntity> transactions = Collections.emptySet();
        oldCategory.setId(oldIdCategory);
        oldUnit.setId(oldIdUnit);
        oldEntity.setId(id);
        oldEntity.setProductCategory(oldCategory);
        oldEntity.setDesignation(oldDesignation);
        oldEntity.setPrix_vente(oldPrixVente);
        oldEntity.setPrix_achat(oldPrixAchat);
        oldEntity.setQuantite(oldQuantite);
        oldEntity.setSeuil(oldSeuil);
        oldEntity.setUnitEntity(oldUnit);
        oldEntity.setTransaction(transactions);
        oldEntity.setCode_barre(code_barre);
        ProductEntity newEntity = new ProductEntity();
        Long newIdCategory = 2L;
        Double newPrixVente = 3000.0;
        Double newPrixAchat = 3500.0;
        int newQuantite = 1000;
        int newSeuil = 5;
        String newDesignation = "moby lait";
        Long newIdUnit = 2L;
        ProductCategoryEntity newCategory = new ProductCategoryEntity();
        UnitEntity newUnit = new UnitEntity();
        newCategory.setId(newIdCategory);
        newUnit.setId(newIdUnit);
        newEntity.setId(id);
        newEntity.setProductCategory(newCategory);
        newEntity.setDesignation(newDesignation);
        newEntity.setQuantite(newQuantite);
        newEntity.setPrix_achat(newPrixAchat);
        newEntity.setPrix_vente(newPrixVente);
        newEntity.setSeuil(newSeuil);
        newEntity.setUnitEntity(newUnit);
        newEntity.setTransaction(transactions);
        newEntity.setCode_barre(code_barre);
        when(repository.findById(id)).thenReturn(Optional.of(oldEntity));
        when(repository.save(Mockito.any(ProductEntity.class))).thenReturn(newEntity);
        //Exécution
        final ProductDto productDto = objectToTest.updateProduct(newEntity);
        //Vérification
        verify(repository).findById(id);
        verify(repository).save(Mockito.argThat(
                new ArgumentMatcher<ProductEntity>() {
                    @Override
                    public boolean matches(ProductEntity entity) {
                        assertEquals(id, entity.getId());
                        assertEquals(newDesignation, entity.getDesignation(), "tests d'entrées de désignation");
                        assertEquals(newPrixAchat, entity.getPrix_achat());
                        return true;
                    }
                }
        ));
        assertAll(() -> assertEquals(id, productDto.getId(), "produits d'Id différents"),
                () -> assertEquals(newDesignation, productDto.getDesignation(), "produits de désignation différentes"),
                () -> assertEquals(newIdCategory, productDto.getIdCategory(), "produits de catégories différentes"),
                () -> assertEquals(newIdUnit, productDto.getIdUnit(), "unités différentes"),
                () -> assertEquals(newPrixAchat, productDto.getPrix_achat(), "prix d'achat différent"),
                () -> assertEquals(newPrixVente, productDto.getPrix_vente(), "prix de vente différents"),
                () -> assertEquals(newQuantite, productDto.getQuantite(), "quantité de produits différents"),
                () -> assertEquals(newSeuil, productDto.getSeuil(), "seuil de produit différents")
        );
    }

    @Test
    public void testThrowUpdateProduct() {
        ProductEntity entity = new ProductEntity();
        String errorMessage = "Produit introuvable";
        Long id = 1L;
        entity.setId(id);
        when(repository.findById(id)).thenReturn(Optional.empty());
        ProductException result =
                assertThrows(ProductException.class, () -> objectToTest.updateProduct(entity));
        assertEquals(errorMessage, result.getMessage(), "Bonne exception produite");
    }

    @Test
    public void testSuccessfullSaveProduct() throws ProductException {
        //préparation
        ProductEntity entity = new ProductEntity();
        Long id = 1L;
        Long idCategory = 1L;
        Double prixVente = 2000.0;
        Double prixAchat = 2500.0;
        int quantite = 500;
        int seuil = 5;
        String designation = "moby bébé";
        Long idUnit = 1L;
        String code_barre = "empty";
        ProductCategoryEntity category = new ProductCategoryEntity();
        UnitEntity unit = new UnitEntity();
        Set<TransactionEntity> transactions = Collections.emptySet();
        category.setId(idCategory);
        unit.setId(idUnit);
        entity.setId(id);
        entity.setProductCategory(category);
        entity.setDesignation(designation);
        entity.setPrix_vente(prixVente);
        entity.setPrix_achat(prixAchat);
        entity.setQuantite(quantite);
        entity.setSeuil(seuil);
        entity.setUnitEntity(unit);
        entity.setTransaction(transactions);
        entity.setCode_barre(code_barre);
        when(categoryRepository.findById(1L)).thenReturn(Optional.of(category));
        when(repository.save(Mockito.any(ProductEntity.class))).thenReturn(entity);
        //Exécution
        ProductDto productDto = objectToTest.saveProduct(entity);
        //Vérification
        assertAll(
                () -> assertEquals(id, productDto.getId(), "produits d'Id différents"),
                () -> assertEquals(designation, productDto.getDesignation(), "produits de désignation différentes"),
                () -> assertEquals(idCategory, productDto.getIdCategory(), "produits de catégories différentes"),
                () -> assertEquals(idUnit, productDto.getIdUnit(), "unités différentes"),
                () -> assertEquals(prixAchat, productDto.getPrix_achat(), "prix d'achat différent"),
                () -> assertEquals(prixVente, productDto.getPrix_vente(), "prix de vente différents"),
                () -> assertEquals(quantite, productDto.getQuantite(), "quantité de produits différents"),
                () -> assertEquals(seuil, productDto.getSeuil(), "seuil de produit différents")
        );
    }
    @Test
    public void testThrowSaveProduct(){
        ProductEntity entity = new ProductEntity();
        ProductCategoryEntity categoryEntity = new ProductCategoryEntity();
        String errorMessage ="Spécifier une catégorie correcte pour ce produit";
        Long id = 1L;
        categoryEntity.setId(id);
        entity.setId(id);
        when(categoryRepository.findById(id)).thenReturn(Optional.empty());
        ProductException result =
                assertThrows(ProductException.class, () -> objectToTest.saveProduct(entity));
        assertEquals(errorMessage, result.getMessage(), "Bonne exception produite");
    }
}