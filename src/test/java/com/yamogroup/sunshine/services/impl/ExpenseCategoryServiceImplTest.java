package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dao.repositories.ExpenseCategoryRepository;
import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.exceptions.ExpenseException;
import com.yamogroup.sunshine.services.mappers.ExpenseCategoryConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseCategoryServiceImplTest {
  @Mock private ExpenseCategoryRepository repository;
  @InjectMocks private ExpenseCategoryServiceImpl objectToTest;

  @Test
  public void testsaveExpenseCategory() {
    final ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
    entity.setPaiement_fixe(120000.0);
    entity.setType("cartable");
    entity.setId(1L);
    when(repository.save(entity)).thenReturn(entity);

    final ExpenseCategoryDto dto = objectToTest.saveExpenseCategory(entity);
    assertAll(
        () -> assertEquals(1L, entity.getId(), "id de même nature"),
        () -> assertEquals("cartable", entity.getType(), "type de même valeur"),
        () -> assertEquals(120000.0, entity.getPaiement_fixe(), "paiement de même valeur"));
  }

  @Test
  public void testSuccessful_findExpenseCategoryById() throws Exception {
    Long accountId = 1L;
    final ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
    entity.setPaiement_fixe(120000.0);
    entity.setType("cartable");
    entity.setId(1L);
    when(repository.findById(accountId)).thenReturn(Optional.of(entity));

    final ExpenseCategoryDto result = objectToTest.findExpenseCategoryById(accountId);
    assertAll(
        () -> assertEquals(1L, result.getId(), "id de même nature"),
        () -> assertEquals("cartable", result.getType(), "type de même valeur"),
        () -> assertEquals(120000.0, result.getPaiement_fixe(), "paiement de même valeur"));
  }

  @Test
  public void testfindAllExpense_Category() {
    final ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
    entity.setPaiement_fixe(120000.0);
    entity.setType("cartable");
    entity.setId(1L);
    List<ExpenseCategoryEntity> entities = new ArrayList<ExpenseCategoryEntity>();
    Collections.addAll(entities, entity);
    when(repository.findAll()).thenReturn(entities);

    final List<ExpenseCategoryDto> result = objectToTest.findAllExpenseCategory();
    assertAll(
        () -> assertFalse(result.isEmpty()),
        () -> assertEquals(1, result.size(), "taille équivalente"),
        () -> assertEquals(1L, result.get(0).getId(), "id de même nature"),
        () -> assertEquals("cartable", result.get(0).getType(), "type de même valeur"),
        () -> assertEquals(120000.0, result.get(0).getPaiement_fixe(), "paiement de même valeur"));
  }

  @Test
  public void testSuccessful_updateExpenseCategory() throws Exception {
    final ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
    entity.setPaiement_fixe(120000.0);
    entity.setType("cartable");
    entity.setId(1L);
    ExpenseCategoryDto dto = new ExpenseCategoryDto();
    dto.setPaiement_fixe(120000.0);
    dto.setType("imprime");
    dto.setId(1L);
    Long accountId = 1L;
    when(repository.findById(dto.getId())).thenReturn(Optional.of(entity));
    when(repository.save(ExpenseCategoryConverter.toEntity(dto)))
        .thenReturn(ExpenseCategoryConverter.toEntity(dto));

    final ExpenseCategoryDto result = objectToTest.updateExpenseCategory(dto);
    verify(repository).findById(dto.getId());
    verify(repository).save(Mockito.any(ExpenseCategoryEntity.class));
    assertAll(
        () -> assertEquals(accountId, result.getId(), "id de même nature"),
        () -> assertEquals(dto.getType(), result.getType(), "type de même valeur"),
        () ->
            assertEquals(
                dto.getPaiement_fixe(), result.getPaiement_fixe(), "paiement de même valeur"));
  }

  @Test
  public void testThrow_findExpenseCategoryById() {
    Long accountId = 1L;
    String errorMessage = "La catégorie de dépense spécifiée n'existe pas";
    when(repository.findById(accountId)).thenReturn(Optional.empty());

    ExpenseException result =
        assertThrows(ExpenseException.class, () -> objectToTest.findExpenseCategoryById(accountId));
    assertEquals(errorMessage, result.getMessage());
  }

  @Test
  public void testThrow_updateExpenseCategory() {
    ExpenseCategoryDto dto = new ExpenseCategoryDto();
    String errorMessage = "Id manquant ou enregistrement manquant pour cette catégorie";
    when(repository.findById(dto.getId())).thenReturn(Optional.empty());

    ExpenseException result =
        assertThrows(ExpenseException.class, () -> objectToTest.updateExpenseCategory(dto));
    assertEquals(errorMessage, result.getMessage());
  }
}
