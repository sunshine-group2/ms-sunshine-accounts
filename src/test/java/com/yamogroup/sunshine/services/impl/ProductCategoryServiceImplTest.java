package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.ProductCategoryEntity;
import com.yamogroup.sunshine.dao.repositories.ProductCategoryRepository;
import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.exceptions.ProductCategoryException;
import com.yamogroup.sunshine.exceptions.ProductException;
import com.yamogroup.sunshine.services.mappers.ProductCategoryConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductCategoryServiceImplTest {

    @Mock
    private ProductCategoryRepository repository;
    @InjectMocks
    private ProductCategoryServiceImpl objectToTest;

    @Test
    public void testSuccessful_findProductCategoryById() throws Exception {
        Long categoryId = 1L;
        final ProductCategoryEntity entity = new ProductCategoryEntity();
        entity.setPaiement_fixe(120000.0);
        entity.setType("cartable");
        entity.setId(1L);
        when(repository.findById(categoryId)).thenReturn(Optional.of(entity));

        final ProductCategoryDto result = objectToTest.findProductCategoryById(categoryId);
        assertAll(
                () -> assertEquals(1L, result.getId(), "id de même nature"),
                () -> assertEquals("cartable", result.getType(), "type de même valeur"),
                () -> assertEquals(120000.0, result.getPaiement_fixe(), "paiement de même valeur"));
    }

    @Test
    public void testThrow_findProductCategoryById() {
        Long categoryId = 1L;
        String errorMessage = "La catégorie de produit spécifiée n'existe pas";
        when(repository.findById(categoryId)).thenReturn(Optional.empty());

        ProductCategoryException result =
                assertThrows(
                        ProductCategoryException.class, () -> objectToTest.findProductCategoryById(categoryId));
        assertEquals(errorMessage, result.getMessage());
    }

    @Test
    public void testSuccessful_findAllCategories() throws Exception {
        // préparation
        int size = 1;
        Long id = 1L;
        String type = "gels";
        Double paiement_fixe = 2500.0;
        ProductCategoryEntity entity = new ProductCategoryEntity();
        entity.setId(id);
        entity.setType(type);
        entity.setPaiement_fixe(paiement_fixe);
        List<ProductCategoryEntity> entities = new ArrayList<ProductCategoryEntity>();
        entities.add(entity);
        when(repository.findAll()).thenReturn(entities);
        // Exécution
        final List<ProductCategoryDto> dtos = objectToTest.findAllCategories();
        // Vérification
        assertAll(
                () -> assertEquals(size, dtos.size(), "taille incorrect"),
                () -> assertEquals(id, dtos.get(0).getId(), "id incorrect"),
                () -> assertEquals(paiement_fixe, dtos.get(0).getPaiement_fixe(), "paiement incorrect"),
                () -> assertEquals(type, dtos.get(0).getType(), "type incorrect"));
    }

    @Test
    public void testSuccessful_updateProductCategory() throws Exception {
        final ProductCategoryEntity entity = new ProductCategoryEntity();
        entity.setPaiement_fixe(120000.0);
        entity.setType("cartable");
        entity.setId(1L);
        ProductCategoryDto dto = new ProductCategoryDto();
        dto.setPaiement_fixe(150000.0);
        dto.setType("imprime");
        dto.setId(1L);
        Long accountId = 1L;
        when(repository.findById(dto.getId())).thenReturn(Optional.of(entity));
        when(repository.save(any(ProductCategoryEntity.class))).thenReturn(ProductCategoryConverter.toEntity(dto));

        final ProductCategoryDto result = objectToTest.updateProductCategory(dto);
        verify(repository).findById(dto.getId());
        verify(repository).save(Mockito.argThat(argument -> {
            assertEquals(dto.getPaiement_fixe(), argument.getPaiement_fixe());
            assertEquals(dto.getType(), argument.getType());
            return true;
        }));
        assertAll(() -> assertEquals(accountId, result.getId(), "id de même nature"),
                () -> assertEquals(dto.getType(), result.getType(), "type de même valeur"),
                () -> assertEquals(dto.getPaiement_fixe(), result.getPaiement_fixe(), "paiement de même valeur")
        );
    }

    @Test
    public void testThrow_updateProductCategory() {
        ProductCategoryDto dto = new ProductCategoryDto();
        String errorMessage = "Id manquant ou enregistrement manquant pour cette catégorie";
        when(repository.findById(dto.getId())).thenReturn(Optional.empty());

        ProductCategoryException result = assertThrows(ProductCategoryException.class, () -> objectToTest.updateProductCategory(dto));
        assertEquals(errorMessage, result.getMessage());

    }

}
