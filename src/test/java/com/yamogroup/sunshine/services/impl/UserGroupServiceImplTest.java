package com.yamogroup.sunshine.services.impl;

import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dao.repositories.UserGroupRepository;
import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.exceptions.UserGroupException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserGroupServiceImplTest {
    @Mock
    public UserGroupRepository repository;
    @InjectMocks
    public UserGroupServiceImpl objectToTest;

    @Test
    public UserGroupDto testSaveUserGroup() throws UserGroupException {
        final UserGroupEntity entity = new UserGroupEntity();
        entity.setLibelle("new group");
        entity.setId(3L);
        when(repository.save(entity)).thenReturn(entity);

        final UserGroupDto dto = objectToTest.saveUserGroup(entity);
        assertAll(
                () -> assertEquals(1L, entity.getId(), "id de même nature"),
                () -> assertEquals("cartable", entity.getLibelle(), "libelle de même valeur"));
        return dto;
    }
}
