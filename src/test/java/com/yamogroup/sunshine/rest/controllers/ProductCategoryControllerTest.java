package com.yamogroup.sunshine.rest.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dto.ProductCategoryDto;
import com.yamogroup.sunshine.services.ProductCategoryService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductCategoryController.class)
public class ProductCategoryControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private ProductCategoryService categoryService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testFindProductCategoryById() throws Exception {
        Long categoryId = 1L;
        ProductCategoryDto dto = new ProductCategoryDto();
        dto.setId(1L);
        dto.setType("fourniture");
        dto.setPaiement_fixe(10000.0);
        when(categoryService.findProductCategoryById(categoryId)).thenReturn(dto);
        mvc.perform(get("/productCategories/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").isNotEmpty(),
                        MockMvcResultMatchers.jsonPath("$.type").value("fourniture"),
                        MockMvcResultMatchers.jsonPath("$.paiement_fixe").value(10000.0));
    }

    @Test
    public void testfindAllCategories() throws Exception {
        // préparation
        int size = 1;
        Long id = 1L;
        String type = "gels";
        double paiement_fixe = 2500.0;
        ProductCategoryDto dto = new ProductCategoryDto();
        dto.setId(id);
        dto.setPaiement_fixe(paiement_fixe);
        dto.setType(type);
        List<ProductCategoryDto> dtos = new ArrayList<>();
        dtos.add(dto);
        when(categoryService.findAllCategories()).thenReturn(dtos);
        // Exécution et vérification
        mvc.perform(get("/productCategories").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.size()").value(size),
                        MockMvcResultMatchers.jsonPath("$[0].id").value(id),
                        MockMvcResultMatchers.jsonPath("$[0].type").value(type),
                        MockMvcResultMatchers.jsonPath("$[0].paiement_fixe").value(paiement_fixe));
    }

    @Test
    public void testUpdateProductCategory() throws Exception {
        ProductCategoryDto dto = new ProductCategoryDto();
        ProductCategoryDto dto1 = new ProductCategoryDto();
        dto.setId(1L);
        dto.setType("fourniture");
        dto.setPaiement_fixe(10000.0);
        dto1.setId(1L);
        dto1.setType("patisserie");
        dto1.setPaiement_fixe(20000.0);
        when(categoryService.updateProductCategory(Mockito.any(ProductCategoryDto.class))).thenReturn(dto1);

        mvc.perform(put("/productCategories")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").value("patisserie"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.paiement_fixe").value(20000.0));
        verify(categoryService).updateProductCategory(Mockito.argThat(productCategoryDto -> {
            assertEquals("fourniture", productCategoryDto.getType());
            assertEquals(10000.0, productCategoryDto.getPaiement_fixe());
            assertEquals(1L, productCategoryDto.getId());
            return true;
        }));
    }

}
