package com.yamogroup.sunshine.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dao.entities.UserGroupEntity;
import com.yamogroup.sunshine.dto.UserGroupDto;
import com.yamogroup.sunshine.services.UserGroupService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(UserGroupController.class)
public class UserGroupControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserGroupService userGroupService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSaveUserGroup() throws Exception {
        Long id = 3L;
        String libelle = "new group";
        Long lawgroupsId = 1L;
        UserGroupDto userGroupDto = new UserGroupDto();
        userGroupDto.setId(id);
        userGroupDto.setLibelle(libelle);
        userGroupDto.setLawgroupsId(lawgroupsId);
        when(userGroupService.saveUserGroup(Mockito.any(UserGroupEntity.class))).thenReturn(userGroupDto);

        mvc.perform(
                        post("/userGroup")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userGroupDto))
                ).andDo(print())
                .andExpectAll(
                        status().isCreated(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").value(id),
                        MockMvcResultMatchers.jsonPath("$.libelle").value(libelle),
                        MockMvcResultMatchers.jsonPath("$.lawgroupsId").value(lawgroupsId)
                );
    }
}
