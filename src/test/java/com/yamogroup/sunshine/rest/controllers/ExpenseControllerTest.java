package com.yamogroup.sunshine.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dao.entities.ExpenseEntity;
import com.yamogroup.sunshine.dto.ExpenseDto;
import com.yamogroup.sunshine.services.ExpenseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ExpenseController.class)
public class ExpenseControllerTest {
  @Autowired private MockMvc mvc;
  @MockBean private ExpenseService expenseService;
  @Autowired private ObjectMapper objectMapper;

  @Test
  public void testsaveExpense() throws Exception {
    // préparation
    Long id = 1L;
    double frais = 2525.0;
    Long accountId = 1L;
    Long categoryId = 1L;
    ExpenseDto expenseDto = new ExpenseDto();
    expenseDto.setId(id);
    expenseDto.setCategoryId(categoryId);
    expenseDto.setAccountId(accountId);
    expenseDto.setFrais(frais);
    when(expenseService.saveExpense(Mockito.any(ExpenseEntity.class))).thenReturn(expenseDto);
    // Exécution et vérification
    mvc.perform(
            post("/expenses")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(expenseDto)))
        .andDo(print())
        .andExpectAll(
            status().isCreated(),
            MockMvcResultMatchers.jsonPath("$").exists(),
            MockMvcResultMatchers.jsonPath("$.id").value(id),
            MockMvcResultMatchers.jsonPath("$.accountId").value(accountId),
            MockMvcResultMatchers.jsonPath("$.categoryId").value(categoryId),
            MockMvcResultMatchers.jsonPath("$.frais").value(frais));
  }

  @Test
  public void testupdateExpense() throws Exception {
    // préparation
    Long id = 1L;
    double oldFrais = 2525.0;
    Long oldAccountId = 1L;
    Long oldCategoryId = 1L;
    ExpenseDto oldDto = new ExpenseDto();
    oldDto.setId(id);
    oldDto.setFrais(oldFrais);
    oldDto.setAccountId(oldAccountId);
    oldDto.setCategoryId(oldCategoryId);

    double newfrais = 2600.0;
    Long newAccountId = 1L;
    Long newCategoryId = 2L;
    ExpenseDto newDto = new ExpenseDto();
    newDto.setId(id);
    newDto.setCategoryId(newCategoryId);
    newDto.setAccountId(newAccountId);
    newDto.setFrais(newfrais);
    when(expenseService.updateExpense(Mockito.any(ExpenseEntity.class))).thenReturn(newDto);
    // Exécution et vérification
    mvc.perform(
            put("/expenses")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(oldDto)))
        .andDo(print())
        .andExpectAll(
            status().isOk(),
            MockMvcResultMatchers.jsonPath("$").exists(),
            MockMvcResultMatchers.jsonPath("$.id").value(id),
            MockMvcResultMatchers.jsonPath("$.accountId").value(newAccountId),
            MockMvcResultMatchers.jsonPath("$.categoryId").value(newCategoryId),
            MockMvcResultMatchers.jsonPath("$.frais").value(newfrais));
  }
}
