package com.yamogroup.sunshine.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dao.entities.AccountEntity;
import com.yamogroup.sunshine.dto.AccountDto;
import com.yamogroup.sunshine.dto.response.AccountDtoResponse;
import com.yamogroup.sunshine.enums.TypeAccount;
import com.yamogroup.sunshine.services.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {
  @Autowired private MockMvc mvc;
  @MockBean private AccountService accountService;
  @Autowired private ObjectMapper objectMapper;

  @Test
  public void test_findAllAccount() throws Exception {
    AccountDtoResponse dto = new AccountDtoResponse();
    AccountDtoResponse dto1 = new AccountDtoResponse();
    List<AccountDtoResponse> dtos = new ArrayList<AccountDtoResponse>();
    dto.setPseudo("ambMevongo");
    dto.setEmail("ambaramevongo@gmail.com");
    dto.setId(1L);
    dto1.setId(2L);
    dto1.setEmail("johnstone@gmail.com");
    dto1.setPseudo("Beric237");
    Collections.addAll(dtos, dto, dto1);

    when(accountService.findAllAccount()).thenReturn(dtos);

    mvc.perform(get("/account").accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2))
        .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(2))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].email").value("ambaramevongo@gmail.com"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[1].email").value("johnstone@gmail.com"));
  }

  @Test
  public void test_getAccountById() throws Exception {
    Long AccountId = 1L;
    AccountDtoResponse dto = new AccountDtoResponse();
    dto.setPseudo("ambMevongo");
    dto.setEmail("ambaramevongo@gmail.com");
    dto.setId(1L);
    when(accountService.getAccountById(AccountId)).thenReturn(dto);

    mvc.perform(get("/account/" + AccountId).accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("ambaramevongo@gmail.com"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.pseudo").value("ambMevongo"));
  }

  @Test
  public void test_findAccountByEmail() throws Exception {
    String email = "ambaramevongo@gmail.com";
    AccountDtoResponse dto = new AccountDtoResponse();
    List<AccountDtoResponse> dtos = new ArrayList<AccountDtoResponse>();
    dto.setPseudo("ambMevongo");
    dto.setEmail("ambaramevongo@gmail.com");
    dto.setId(1L);
    when(accountService.findAccountByEmail(email)).thenReturn(dto);

    mvc.perform(get("/account/email/" + email).accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("ambaramevongo@gmail.com"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.pseudo").value("ambMevongo"));
  }

  @Test
  public void test_listAccountByType() throws Exception {
    TypeAccount type = TypeAccount.valueOf("CUSTOMER");
    AccountDtoResponse dto = new AccountDtoResponse();
    AccountDto dto1 = new AccountDto();
    List<AccountDtoResponse> dtos = new ArrayList<AccountDtoResponse>();
    dto.setPseudo("ambMevongo");
    dto.setEmail("ambaramevongo@gmail.com");
    dto.setId(1L);
    Collections.addAll(dtos, dto);

    when(accountService.listAccountByType(type)).thenReturn(dtos);
    mvc.perform(get("/account/type/" + type).accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].pseudo").value("ambMevongo"))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$[0].email").value("ambaramevongo@gmail.com"));
  }

  @Test
  public void test_saveAccount() throws Exception {
    AccountDtoResponse dto = new AccountDtoResponse();
    AccountEntity entity = new AccountEntity();
    dto.setPseudo("ambMevongo");
    dto.setEmail("ambaramevongo@gmail.com");
    dto.setId(1L);

    entity.setNom("Ambara");
    entity.setPrenom("Mevongo");
    entity.setPseudo("ambMevongo");
    entity.setEmail("ambaramevongo@gmail.com");
    entity.setId(1L);
    entity.setPassword("BLEM1405");

    when(accountService.saveAccount(Mockito.any(AccountEntity.class))).thenReturn(dto);

    mvc.perform(
            post("/account")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(entity))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.pseudo").value("ambMevongo"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("ambaramevongo@gmail.com"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andDo(print());
    verify(accountService)
        .saveAccount(
            Mockito.argThat(
                new ArgumentMatcher<AccountEntity>() {
                  @Override
                  public boolean matches(AccountEntity accountEntity) {
                    assertEquals("Ambara", entity.getNom());
                    assertEquals("Mevongo", entity.getPrenom());
                    assertEquals(1L, entity.getId());
                    return true;
                  }
                }));
  }

  @Test
  public void test_updateAccount() throws Exception {
    AccountDtoResponse dto = new AccountDtoResponse();
    AccountEntity entity = new AccountEntity();
    dto.setPseudo("steph30");
    dto.setEmail("stephencurry@gmail.com");
    dto.setId(1L);
    entity.setNom("Ambara");
    entity.setPrenom("Mevongo");
    entity.setPseudo("ambMevongo");
    entity.setEmail("ambaramevongo@gmail.com");
    entity.setId(1L);
    entity.setPassword("BLEM1405");
    when(accountService.updateAccount(Mockito.any(AccountEntity.class))).thenReturn(dto);

    mvc.perform(
            put("/account")
                .content(objectMapper.writeValueAsString(entity))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
        .andExpect(MockMvcResultMatchers.jsonPath("$.pseudo").value("steph30"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("stephencurry@gmail.com"))
        .andDo(print());
    verify(accountService)
        .updateAccount(
            Mockito.argThat(
                new ArgumentMatcher<AccountEntity>() {
                  @Override
                  public boolean matches(AccountEntity accountEntity) {
                    assertEquals("Ambara", accountEntity.getNom());
                    assertEquals("Mevongo", accountEntity.getPrenom());
                    assertEquals("ambMevongo", accountEntity.getPseudo());
                    assertEquals("BLEM1405", accountEntity.getPassword());
                    return true;
                  }
                }));
  }
}
