package com.yamogroup.sunshine.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dao.entities.ExpenseCategoryEntity;
import com.yamogroup.sunshine.dto.ExpenseCategoryDto;
import com.yamogroup.sunshine.services.ExpenseCategoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ExpenseCategoryController.class)
class ExpenseCategoryControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ExpenseCategoryService categoryService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSaveExpense_Category() throws Exception {
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
        Long id = 1L;
        String type = "fourniture";
        double paiement_fixe = 10000.0;
        dto.setId(id);
        dto.setType(type);
        dto.setPaiement_fixe(paiement_fixe);
        entity.setId(id);
        entity.setType(type);
        entity.setPaiement_fixe(paiement_fixe);
        when(categoryService.saveExpenseCategory(entity)).thenReturn(dto);

        mvc.perform(
                        post("/categoryExpenses")
                                .accept(MediaType.APPLICATION_JSON)
                                .content("{\"id\":\"1\",\"type\":\"fourniture\",\"paiement_fixe\":\"10000.0\"}")
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpectAll(
                        status().isCreated(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").value(id),
                        MockMvcResultMatchers.jsonPath("$.type").value(type),
                        MockMvcResultMatchers.jsonPath("$.paiement_fixe").value(paiement_fixe));
    }

    @Test
    public void testFindExpense_CategoryById() throws Exception {
        Long accountId = 1L;
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        dto.setId(1L);
        dto.setType("fourniture");
        dto.setPaiement_fixe(10000.0);
        when(categoryService.findExpenseCategoryById(accountId)).thenReturn(dto);
        mvc.perform(get("/categoryExpenses/1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").isNotEmpty(),
                        MockMvcResultMatchers.jsonPath("$.type").value("fourniture"),
                        MockMvcResultMatchers.jsonPath("$.paiement_fixe").value(10000.0));
    }

    @Test
    public void testUpdateExpense_Category() throws Exception {
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        ExpenseCategoryDto dto1 = new ExpenseCategoryDto();
        dto.setId(1L);
        dto.setType("fourniture");
        dto.setPaiement_fixe(10000.0);
        dto1.setId(1L);
        dto1.setType("patisserie");
        dto1.setPaiement_fixe(20000.0);
        when(categoryService.updateExpenseCategory(Mockito.any(ExpenseCategoryDto.class)))
                .thenReturn(dto1);

        mvc.perform(
                        put("/categoryExpenses")
                                .content(objectMapper.writeValueAsString(dto))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.type").value("patisserie"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.paiement_fixe").value(20000.0));
        verify(categoryService)
                .updateExpenseCategory(
                        Mockito.argThat(
                                expenseCategoryDto -> {
                                    assertEquals("fourniture", expenseCategoryDto.getType());
                                    assertEquals(10000.0, expenseCategoryDto.getPaiement_fixe());
                                    assertEquals(1L, expenseCategoryDto.getId());
                                    return true;
                                }));
    }

    @Test
    void testfindAllExpense_Category() throws Exception {
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        ExpenseCategoryDto dto1 = new ExpenseCategoryDto();
        List<ExpenseCategoryDto> dtos = new ArrayList<>();
        dto.setId(1L);
        dto.setType("fourniture");
        dto.setPaiement_fixe(10000.0);
        dto1.setId(2L);
        dto1.setType("patisserie");
        dto1.setPaiement_fixe(20000.0);
        Collections.addAll(dtos, dto, dto1);
        when(categoryService.findAllExpenseCategory()).thenReturn(dtos);

        mvc.perform(get("/categoryExpenses").accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        content().contentType("application/json"),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$[*].id").isNotEmpty(),
                        MockMvcResultMatchers.jsonPath("$.size()").value(2),
                        MockMvcResultMatchers.jsonPath("$[0].type").value("fourniture"),
                        MockMvcResultMatchers.jsonPath("$[1].type").value("patisserie"));
    }
}
