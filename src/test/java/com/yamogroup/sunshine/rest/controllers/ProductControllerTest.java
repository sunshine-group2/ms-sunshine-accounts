package com.yamogroup.sunshine.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.sunshine.dao.entities.ProductEntity;
import com.yamogroup.sunshine.dto.ProductDto;
import com.yamogroup.sunshine.services.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ProductService productService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testUpdateProduct() throws Exception {
        //preparation
        ProductDto oldDto = new ProductDto();
        Set<Long> transactions = Collections.emptySet();
        Long id = 1L;
        int oldQuantite = 500;
        Long oldIdUnit = 1L;
        Long oldIdCategory = 1L;
        String oldDesignation = "moby lait";
        Double oldPrixAchat = 2550.0;
        Double oldPrixVente = 2575.0;
        String codeBarre = "empty";
        int oldSeuil = 10;

        oldDto.setId(id);
        oldDto.setQuantite(oldQuantite);
        oldDto.setIdUnit(oldIdUnit);
        oldDto.setIdCategory(oldIdCategory);
        oldDto.setDesignation(oldDesignation);
        oldDto.setPrix_achat(oldPrixAchat);
        oldDto.setPrix_vente(oldPrixVente);
        oldDto.setCode_barre(codeBarre);
        oldDto.setTransaction(transactions);
        oldDto.setSeuil(oldSeuil);

        ProductDto newDto = new ProductDto();
        int newQuantite = 1500;
        Long newIdUnit = 2L;
        Long newIdCategory = 2L;
        String newDesignation = "moby bébé";
        Double newPrixAchat = 3000.0;
        Double newPrixVente = 3500.0;
        int newSeuil = 15;
        newDto.setId(id);
        newDto.setQuantite(newQuantite);
        newDto.setIdUnit(newIdUnit);
        newDto.setIdCategory(newIdCategory);
        newDto.setDesignation(newDesignation);
        newDto.setPrix_achat(newPrixAchat);
        newDto.setPrix_vente(newPrixVente);
        newDto.setCode_barre(codeBarre);
        newDto.setTransaction(transactions);
        newDto.setSeuil(newSeuil);
        when(productService.updateProduct(Mockito.any(ProductEntity.class))).thenReturn(newDto);
        //Vérification et exécution
        mvc.perform(
                        put("/products")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(oldDto))
                ).andDo(print())
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").value(id),
                        MockMvcResultMatchers.jsonPath("$.seuil").value(newSeuil),
                        MockMvcResultMatchers.jsonPath("$.designation").value(newDesignation),
                        MockMvcResultMatchers.jsonPath("$.prix_vente").value(newPrixVente),
                        MockMvcResultMatchers.jsonPath("$.prix_achat").value(newPrixAchat)
                );

    }

    @Test
    public void testSaveProduct() throws Exception {
        //preparation
        ProductDto productDto = new ProductDto();
        Set<Long> transactions = Collections.emptySet();
        Long id = 1L;
        int quantite = 500;
        Long idUnit = 1L;
        Long idCategory = 1L;
        String designation = "moby lait";
        Double prixAchat = 2550.0;
        Double prixVente = 2575.0;
        String codeBarre = "empty";
        int seuil = 10;
        productDto.setId(id);
        productDto.setQuantite(quantite);
        productDto.setIdUnit(idUnit);
        productDto.setIdCategory(idCategory);
        productDto.setDesignation(designation);
        productDto.setPrix_achat(prixAchat);
        productDto.setPrix_vente(prixVente);
        productDto.setCode_barre(codeBarre);
        productDto.setTransaction(transactions);
        productDto.setSeuil(seuil);
        when(productService.saveProduct(Mockito.any(ProductEntity.class))).thenReturn(productDto);
        //Vérification et exécution
        mvc.perform(
                        post("/products")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(productDto))
                ).andDo(print())
                .andExpectAll(
                        status().isOk(),
                        MockMvcResultMatchers.jsonPath("$").exists(),
                        MockMvcResultMatchers.jsonPath("$.id").value(id),
                        MockMvcResultMatchers.jsonPath("$.seuil").value(seuil),
                        MockMvcResultMatchers.jsonPath("$.designation").value(designation),
                        MockMvcResultMatchers.jsonPath("$.prix_vente").value(prixVente),
                        MockMvcResultMatchers.jsonPath("$.prix_achat").value(prixAchat)
                );
    }
}